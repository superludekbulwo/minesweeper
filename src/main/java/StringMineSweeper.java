import java.util.Optional;

public class StringMineSweeper implements MineSweeper {
    private MineFieldValidator mineFieldValidator;
    private MineFieldHint mineFieldHint;
    private Optional<String> mineField;

    public StringMineSweeper(MineFieldValidator mineFieldValidator, MineFieldHint mineFieldHint) {
        this.mineFieldValidator = mineFieldValidator;
        this.mineFieldHint = mineFieldHint;
        this.mineField = Optional.empty();
    }

    public void setMineField(String mineField) throws IllegalArgumentException {
        ValidationResult validationResult = mineFieldValidator.validateMineField(mineField);

        if (!validationResult.isValid())
            throw new IllegalArgumentException(validationResult.getError().orElse("Unknown error occurred"));

        this.mineField = Optional.of(mineField);
    }

    public String getHintField() throws IllegalStateException {
        return mineFieldHint.getHint(mineField.orElseThrow(() -> new IllegalStateException()));
    }
}
