import java.util.Optional;

public abstract class ValidationResult {
    public abstract boolean isValid();

    public abstract Optional<String> getError();

    public static ValidationResult Valid() {
        return new ValidResult();
    }

    public static ValidationResult Invalid(String error) {
        return new InvalidResult(error);
    }
}