import java.util.Optional;

public class InvalidResult extends ValidationResult {
    private Optional<String> error;

    protected InvalidResult(String error) {
        if (error == null || error.isEmpty())
            throw new IllegalArgumentException("messages contains invalid value");
        this.error = Optional.of(error);
    }

    @Override
    public boolean isValid() {
        return false;
    }

    @Override
    public Optional<String> getError() {
        return error;
    }
}
