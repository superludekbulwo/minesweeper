import org.apache.commons.lang3.StringUtils;

import java.util.Optional;

public class MineFieldHint {
    public String getHint(String mineField) {
        if(mineField == null || mineField.isEmpty())
            throw new IllegalArgumentException("mineField cannot be null or empty");

        final Board board = new Board(mineField);
        final StringBuilder result = new StringBuilder(mineField.length());

        for (int position = 0; position < mineField.length(); ++position) {
            final char characterAtPosition = mineField.charAt(position);
            switch (characterAtPosition) {
                case '*':
                case '\n':
                    result.append(characterAtPosition);
                    break;
                case '.':
                    result.append(getNumberOfMinesInNeibourhood(board, position));
                    break;
                default:
                    throw new IllegalArgumentException("mineField contains invalid character");
            }
        }

        return result.toString();
    }

    private int getNumberOfMinesInNeibourhood(Board board, int position) {
        final String mineField = board.mineField;
        final int numberOfMinesInUpperRowFragment = getNumberOfMinesInMineFieldFragment(mineField, position - 1 - board.columnLenght, position + 2 - board.columnLenght);
        final int numberOfMinesInMiddleRowFragment = getNumberOfMinesInMineFieldFragment(mineField, position - 1, position + 2);
        final int numberOfMinesInLowerRowFragment = getNumberOfMinesInMineFieldFragment(mineField, position - 1 + board.columnLenght, position + 2 + board.columnLenght);

        return numberOfMinesInUpperRowFragment + numberOfMinesInMiddleRowFragment + numberOfMinesInLowerRowFragment;
    }

    private int getNumberOfMinesInMineFieldFragment(String mineField, int start, int end) {
        int maxPosition = mineField.length();
        int minPosition = 0;
        if (end < minPosition || start > maxPosition || start > end)
            return 0;

        int fixedStart = Math.max(minPosition, start);
        int fixedEnd = Math.min(maxPosition, end);
        String mineFieldSubstring = mineField.substring(fixedStart, fixedEnd);

        return StringUtils.countMatches(mineFieldSubstring, '*');
    }

    private class Board {
        private final String mineField;
        private final int columnLenght;

        public Board(String mineField) {
            this.mineField = mineField;
            String[] mineFieldArray = mineField.split("\n");
            this.columnLenght = mineFieldArray[0].length() + 1;
        }
    }
}
