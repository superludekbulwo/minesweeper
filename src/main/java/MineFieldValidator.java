import java.util.Arrays;
import java.util.function.Predicate;
import java.util.regex.Pattern;

public class MineFieldValidator {

    public ValidationResult validateMineField(String mineField) {
        if (mineField == null)
            return ValidationResult.Invalid("mineField cannot be null");

        if (mineField.isEmpty())
            return ValidationResult.Invalid("mineField cannot be empty");

        if (mineField.trim().isEmpty())
            return ValidationResult.Invalid("mineField cannot contain only whitespaces");

        if (isMineFieldContainsInvalidCharacters(mineField))
            return ValidationResult.Invalid("mineField cannot contain invalid characters");

        if (isMineFieldContainsRowsWithUnevenNumberOfCharacters(mineField))
            return ValidationResult.Invalid("mineField cannot contain rows with uneven number of characters");

        return ValidationResult.Valid();
    }

    private boolean isMineFieldContainsInvalidCharacters(String mineField) {
        return Pattern.compile("[^.*\n]").asPredicate().test(mineField);
    }

    private boolean isMineFieldContainsRowsWithUnevenNumberOfCharacters(String mineField) {
        String[] mineFieldsRows = mineField.split("\n");
        return !Arrays.stream(mineFieldsRows)
                .map(row -> row.length())
                .allMatch(Predicate.isEqual(mineFieldsRows[0].length()));
    }
}
