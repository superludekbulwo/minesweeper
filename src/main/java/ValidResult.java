import java.util.Optional;

public class ValidResult extends ValidationResult {
    protected ValidResult() {
    }

    @Override
    public boolean isValid() {
        return true;
    }

    @Override
    public Optional<String> getError() {
        return Optional.empty();
    }
}
