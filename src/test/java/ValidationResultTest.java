import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ValidationResultTest {

    @Test
    public void checkingValidationResult_WhenCreatedValidResult_ShouldReturnTrue() {
        ValidationResult result = ValidationResult.Valid();

        assertThat(result.isValid()).isTrue();
    }

    @Test
    public void checkingValidationResult_WhenCreatedInvalidResult_ShouldReturnFalse() {
        ValidationResult result = ValidationResult.Invalid("SampleValue");

        assertThat(result.isValid()).isFalse();
    }

    @Test
    public void gettingError_WhenCreatedValidResult_ShouldReturnEmptyOptional() {
        ValidationResult result = ValidationResult.Valid();

        assertThat(result.getError()).isEmpty();
    }

    @Test
    public void gettingError_WhenCreatedInvalidResult_ShouldReturnError() {
        ValidationResult result = ValidationResult.Invalid("SampleValue");

        assertThat(result.getError().get()).isEqualTo("SampleValue");
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void creatingInvalidResult_WhenPassingNull_ShouldThrowIllegalArgumentException() {
        ValidationResult.Invalid(null);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void creatingInvalidResult_WhenPassingEmptyString_ShouldThrowIllegalArgumentException() {
        ValidationResult.Invalid("");
    }
}
