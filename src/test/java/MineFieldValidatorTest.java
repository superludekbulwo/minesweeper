import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class MineFieldValidatorTest {
    private MineFieldValidator mineFieldValidator;

    @BeforeMethod
    public void setUp() {
        mineFieldValidator = new MineFieldValidator();
    }

    @Test
    public void validatingMineField_WhenPassingNull_ShouldReturnInvalidResult() {
        ValidationResult result = mineFieldValidator.validateMineField(null);

        assertThat(result.isValid()).isFalse();
    }

    @Test
    public void validatingMineField_WhenPassingEmptyString_ShouldReturnInvalidResult() {
        ValidationResult result = mineFieldValidator.validateMineField("");

        assertThat(result.isValid()).isFalse();
    }

    @Test
    public void validatingMineField_WhenPassingStringContainingOnlyNewLineCharacters_ShouldReturnInvalidResult() {
        ValidationResult result = mineFieldValidator.validateMineField("\n\n");

        assertThat(result.isValid()).isFalse();
    }

    @Test
    public void validatingMineField_WhenPassingStringContainingInvalidCharacter_ShouldReturnInvalidResult() {
        ValidationResult result = mineFieldValidator.validateMineField("..*\n. *");

        assertThat(result.isValid()).isFalse();
    }

    @Test
    public void validatingMineField_WhenPassingStringContainingRowsWithUnevenNumberOfCharacters_ShouldReturnInvalidResult() {
        ValidationResult result = mineFieldValidator.validateMineField("..*\n.**.");

        assertThat(result.isValid()).isFalse();
    }

    @Test
    public void validatingMineField_WhenPassingValidMineField_ShouldReturnValidResult() {
        ValidationResult result = mineFieldValidator.validateMineField("*...\n..*.\n....");

        assertThat(result.isValid()).isTrue();
    }
}
