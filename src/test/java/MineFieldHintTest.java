import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class MineFieldHintTest {
    private MineFieldHint mineFieldHint;

    @BeforeMethod
    public void setUp() {
        mineFieldHint = new MineFieldHint();
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void calculatingFieldHint_WhenPassingNull_ShouldThrowIllegalArgumentException() {
        mineFieldHint.getHint(null);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void calculatingFieldHint_WhenPassingEmptyString_ShouldThrowIllegalArgumentException() {
        mineFieldHint.getHint("");
    }

    @Test
    public void calculatingFieldHint_WhenPassingSingleMine_ShouldReturnMine() {
        String result = mineFieldHint.getHint("*");

        assertThat(result).isEqualTo("*");
    }

    @Test
    public void calculatingFieldHint_WhenPassingSingleEmptyField_ShouldReturnEmptyField() {
        String result = mineFieldHint.getHint(".");

        assertThat(result).isEqualTo("0");
    }

    @Test
    public void calculatingFieldHint_WhenPassingCorrectMineField_ShouldReturnCorrectResult() {
        String result = mineFieldHint.getHint("*...\n..*.\n....");

        assertThat(result).isEqualTo("*211\n12*1\n0111");
    }
}
