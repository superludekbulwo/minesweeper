import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class StringMineSweeperTest {
    private StringMineSweeper mineSweeper;
    private final static String CORRECT_VALUE = "*...\n..*.\n....";
    private final static String INCORRECT_VALUE = "..*.\n...\n*.*";
    private final static String EXPECTED_RESULT = "*211\n12*1\n0111";

    @BeforeMethod
    public void setUp() {
        MineFieldValidator mineFieldValidator = getMineFieldValidator();
        MineFieldHint mineFieldHint = getMineFieldHint();

        mineSweeper = new StringMineSweeper(mineFieldValidator, mineFieldHint);
    }

    private MineFieldHint getMineFieldHint() {
        MineFieldHint mineFieldHint = mock(MineFieldHint.class);

        when(mineFieldHint.getHint(CORRECT_VALUE)).thenReturn(EXPECTED_RESULT);
        return mineFieldHint;
    }

    private MineFieldValidator getMineFieldValidator() {
        MineFieldValidator mineFieldValidator = mock(MineFieldValidator.class);

        ValidationResult validResult = getResult(true, Optional.empty());
        when(mineFieldValidator.validateMineField(CORRECT_VALUE)).thenReturn(validResult);

        ValidationResult invalidResult = getResult(false, Optional.of("Error description"));
        when(mineFieldValidator.validateMineField(INCORRECT_VALUE)).thenReturn(invalidResult);

        return mineFieldValidator;
    }

    private static ValidationResult getResult(boolean isValid, Optional<String> errorDescription) {
        ValidationResult validationResult = mock(ValidationResult.class);
        when(validationResult.isValid()).thenReturn(isValid);
        when(validationResult.getError()).thenReturn(errorDescription);

        return validationResult;
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void settingMineField_WhenValidatorReturnedInvalidResult_ShouldThrowIllegalArgumentException() {
        mineSweeper.setMineField(INCORRECT_VALUE);
    }

    @Test(expectedExceptions = IllegalStateException.class)
    public void gettingHintField_WhenMineFieldIsNotSet_ShouldThrowIllegalStateException() {
        mineSweeper.getHintField();
    }

    @Test
    public void gettingHintField_WhenMineFieldIsSetCorrectly_ShouldReturnCorrectResult() {
        mineSweeper.setMineField(CORRECT_VALUE);
        final String result = mineSweeper.getHintField();

        assertThat(result).isEqualTo(EXPECTED_RESULT);
    }

}